package org.example.relations.Entity;

import jakarta.persistence.*;

@Entity
@Table(name = "children")

public class Child {
    @Id
    private Integer id;

    private String name;

    // presupunem ca fiecare copil are o mancare favorita
    // presupunem ca fiecare mancare este favorita unui singur copil
    @OneToOne
    private Food favoriteFood;
    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Toy favoriteToy;

    public Child(Integer id, String name, Food favoriteFood, Toy favoriteToy) {
        this.id = id;
        this.name = name;
        this.favoriteFood = favoriteFood;
        this.favoriteToy = favoriteToy;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Food getFavoriteFood() {
        return favoriteFood;
    }

    public void setFavoriteFood(Food favoriteFood) {
        this.favoriteFood = favoriteFood;
    }

    public Toy getFavoriteToy() {
        return favoriteToy;
    }

    public void setFavoriteToy(Toy favoriteToy) {
        this.favoriteToy = favoriteToy;
    }
}

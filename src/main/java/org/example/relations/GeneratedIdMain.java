package org.example.relations;

import org.example.Database.DatabaseConfig;
import org.example.relations.Entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class GeneratedIdMain {
    public static void main(String[] args) {
       /* SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Child.class)
                .addAnnotatedClass(Mother.class)
                .addAnnotatedClass(Food.class)
                .addAnnotatedClass(Toy.class)
                .addAnnotatedClass(Hobby.class)
                .buildSessionFactory();*/

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Food chips = new Food(1, "chips", false);
        session.persist(chips);

        // session.refresh(chips);

        Mother mom = new Mother(null, "Mami", Job.ENGINEER);
        Mother secondMom = new Mother(null, "MamiCa", Job.LAWYER);
        System.out.println(secondMom.getId());
        session.persist(mom);
        session.persist(secondMom);
        System.out.println(secondMom.getId());

        transaction.commit();
        session.close();

    }
}

package org.example.ex2;

import org.example.Database.DatabaseConfig;
import org.example.ex1.Movie;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Ex2 {
    public static void main(String[] args) {

      /*  SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Student.class)
                .buildSessionFactory(); */

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();

        Student student1 = new Student(12, "Mihai", 1995);
        session.persist(student1); //ptr a salva studentul in baza de date

        t1.commit();
        session.close();
    }
}

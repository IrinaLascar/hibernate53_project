package org.example.Database;

import org.example.entity.Car;
import org.example.entity.Driver;
import org.example.entity.Truck;
import org.example.ex1.Actor;
import org.example.ex1.Movie;
import org.example.ex2.Teacher;
import org.example.relations.Entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DatabaseConfig {
    private static SessionFactory sessionFactory = null;
    private DatabaseConfig() {

    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null){
            sessionFactory = new Configuration()
                    .configure("hibernate.config.xml")
                    .addAnnotatedClass(Car.class)
                    .addAnnotatedClass(Driver.class)
                    .addAnnotatedClass(Truck.class)
                    .addAnnotatedClass(Actor.class)
                    .addAnnotatedClass(Movie.class)
                    .addAnnotatedClass(Teacher.class)
                    .addAnnotatedClass(Child.class)
                    .addAnnotatedClass(Mother.class)
                    .addAnnotatedClass(Food.class)
                    .addAnnotatedClass(Toy.class)
                    .addAnnotatedClass(Hobby.class)
                    .addAnnotatedClass(TvShow.class)
                    .addAnnotatedClass(Animal.class)
                    .addAnnotatedClass(Owner.class)
                    .buildSessionFactory();
        }
        return sessionFactory;
    }
}

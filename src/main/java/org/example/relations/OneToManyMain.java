package org.example.relations;

import org.example.Database.DatabaseConfig;
import org.example.ex1.Genre;
import org.example.relations.Entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.util.List;

public class OneToManyMain {
    public static void main(String[] args) {
       /* SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Mother.class)
                .addAnnotatedClass(Hobby.class)
                .buildSessionFactory();*/

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction t1 = session.beginTransaction();

        Hobby h1 = new Hobby(null, "Tennis", "beginner");
        Hobby h2 = new Hobby(null, "Chess", "intermediate");
        TvShow serial1 = new TvShow(null, "Serial 1", Genre.COMEDY);
        TvShow dosareleX = new TvShow(null, "Dosarele X", Genre.ACTION);

        Mother m1 = new Mother(null, "Ioana", Job.LAWYER, List.of(h1, h2), List.of(serial1, dosareleX));
        session.persist(h1);
        session.persist(h2);
        session.persist(m1);

        t1.commit();
        session.close();
    }
}

package org.example.relations;

import org.example.Database.DatabaseConfig;
import org.example.relations.Entity.Child;
import org.example.relations.Entity.Food;
import org.example.relations.Entity.Mother;
import org.example.relations.Entity.Toy;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class OneToOneMain {
    public static void main(String[] args) {
      /*  SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Child.class)
                .addAnnotatedClass(Mother.class)
                .addAnnotatedClass(Food.class)
                .addAnnotatedClass(Toy.class)
                .buildSessionFactory();*/

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction1 = session.beginTransaction();

        Food f1 = new Food(1, "milk", true);
        session.persist(f1);
        Toy toy1 = new Toy(3, "Barbie");
//        session.persist(toy1); nu trebuie făcut din cauză că avem cascade
        Child c1 = new Child(1, "Johnny", f1, toy1);
        session.persist(c1);
        transaction1.commit();
        System.out.println("Child was saved");

        Transaction t2 = session.beginTransaction();
        session.remove(c1);
        t2.commit();
        System.out.println("Child was removed");
        System.out.println("Check what happened with toy");

        session.close();
    }
}

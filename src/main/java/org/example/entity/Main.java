package org.example.entity;

import org.example.Database.DatabaseConfig;
import org.example.entity.Car;
import org.example.entity.Driver;
import org.example.entity.Truck;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Main {
    public static void main(String[] args) {

       /* SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Car.class)
                .addAnnotatedClass(Truck.class)
                .addAnnotatedClass(Driver.class)
                .buildSessionFactory(); */

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Car c = new Car();
        c.setId(1);
        c.setName("BMW");
        session.persist(c); // face insert in tabel

        Truck t = new Truck();
        t.setId(21);
        t.setName("testTruck");
        session.persist(t);

        Driver d = new Driver();
        d.setId(3);
        d.setName("John");
        d.setEmail("john@yahoo.com");
        d.setMaxTravelDistance(450);
        d.setSecurityKey("key");
        session.persist(d);

        transaction.commit(); //toate modificarile se transmit in baza de date
        session.close();


    }
}
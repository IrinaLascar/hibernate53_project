package org.example.relations;

import org.example.Database.DatabaseConfig;
import org.example.relations.Entity.Animal;
import org.example.relations.Entity.Owner;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.List;

public class ManyToOneMain {
    public static void main(String[] args) {
        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction1 = session.beginTransaction();
        Owner o1 = new Owner(null, "Mihai", new ArrayList<>());
        Animal cat = new Animal(null, "Tom", "cat", o1);
        o1.addPet(cat);
        session.persist(cat);

        transaction1.commit();
        session.close();
    }
}

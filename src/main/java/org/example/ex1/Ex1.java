package org.example.ex1;

import org.example.Database.DatabaseConfig;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class Ex1 {
    public static void main(String[] args) {
        // adaugati 2 filme si 3 actori
        // creare session factory
        // creare tabele
        // inserare date

       /* SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.config.xml")
                .addAnnotatedClass(Movie.class)
                .buildSessionFactory(); */

        SessionFactory sessionFactory = DatabaseConfig.getSessionFactory();

        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Movie movie1 = new Movie();
        movie1.setId(11345);
        movie1.setName("OriceNume");
        movie1.setGen(Genre.ROMANCE);
        session.persist(movie1);

        transaction.commit(); //toate modificarile se transmit in baza de date
        session.close();
    }
}
